# Coronavirus stats update script

## Introduction

* Shows the Coronavirus progress updated from corona-stats.online

## Usage

* Show help

`$ coronavirus -h`

* Show table with all countries

`$ coronavirus`

* Show table with selected country

`$ coronavirus -c Brazil`


## Author

* Author: Prof. Dr. Ruben Carlo Benante
* Email: rcb@upe.br
* Date: 2020-03-17
* License: GNU/GPL v2.0

